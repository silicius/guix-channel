Random Guix Channel
===================
This repository contains a package collection for Guix package manager[^1]
that could not be in the official distribution or are waiting to get in.

I use it with the GuixSD distribution, so I can not personally provide any
guarantee that it will work elsewhere.


Channel
-------
I advise to read the documentation about channels[^2] before anything.

This channel is indented to be used as an addition
to the official `Guix` channel, it would not work without it.

To use this channel with Guix, you need to write to
`~/.config/guix/channels.scm` these lines:
```guile
(cons (channel
        (name 'random-channel)
        (url "https://gitlab.com/silicius/guix-channel")
      %default-channels)
```
And execute the `guix pull` command.


Community
---------
I will gladly accept new packages or corrections/additions to any file here.
If I will deny some change I will do my best to explain why.

Feel free to contact me at silicius@cock.li.



[^1]: https://www.gnu.org/software/guix/manual/en/html_node/Package-Management.html
[^2]: https://www.gnu.org/software/guix/manual/en/html_node/Channels.html
