;;;    Copyright 2019 silicius <silicius@cock.li>

;;;    This program is free software: you can redistribute it and/or modify
;;;    it under the terms of the GNU General Public License as published by
;;;    the Free Software Foundation, either version 3 of the License, or
;;;    (at your option) any later version.
;;;
;;;    This program is distributed in the hope that it will be useful,
;;;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;    GNU General Public License for more details.
;;;
;;;    You should have received a copy of the GNU General Public License
;;;    along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (winetricks)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build gnu-build-system)
  #:use-module (guix build utils)
  #:use-module (gnu packages wine))


(define-public winetricks
  (let ((commit "2a0dfb0f8ea7e552e70c8459b55ee79a431cd71d")
        (revision "1")
        (last-stable "20181203"))
    (package
     (name "winetricks")
     (version (git-version last-stable revision commit))
     (home-page "https://github.com/Winetricks/winetricks")

     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/Winetricks/winetricks.git")
                    (commit commit)))
              (sha256
               (base32 "0hnjism81fgi46s2b6xdsqphgpmif5h6vlg99anfj8hsbpbk2nza"))
              (file-name (git-file-name name version))))

     (build-system gnu-build-system)
     (arguments `(#:phases (modify-phases %standard-phases
                                          (delete 'configure))
                  #:tests? #f
                  #:make-flags `(,(string-append "PREFIX="
                                                 (assoc-ref %outputs "out")))))

     (propagated-inputs `(("wine" ,wine)))

     (synopsis "Work around problems and install applications under Wine")
     (description "It lets you install missing DLLs or tweak various Wine settings individually.
It also has a menu of supported games/apps for which it can do all the workarounds automatically.")

     (license license:gpl2))))
